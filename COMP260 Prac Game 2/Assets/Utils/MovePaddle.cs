﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {
	private Rigidbody rigidbody;
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}
	private Vector3 GetMousePositon() {
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		Plane plane = new Plane (Vector3.forward, Vector3.zero);
		float distance = 0;
		plane.Raycast (ray, out distance);
		return ray.GetPoint (distance);
	}
	void OnDrawGizmos(){
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePositon ();
		Gizmos.DrawLine (Camera.main.transform.position, pos);
	}
	public Vector3 velocity;
	public float speed = 20f;
	public float force = 20f;
	void FixedUpdate(){
		Vector3 direction;

		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");
		Vector3 Velocity = direction * speed;


//		Vector3 pos = GetMousePositon();
//	Vector3 dir = pos - rigidbody.position;
//		Vector3 vel = dir.normalized * speed;
//		float move = speed * Time.fixedDeltaTime;
//		float distTotarget = dir.magnitude;
//
//		if (move > distTotarget) {
//			vel = vel * distTotarget / move;
//		}
		rigidbody.velocity = Velocity;
	}

}
